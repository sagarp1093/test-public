import java.util.Scanner;

//Just a comment and nothing more.
public class JavaLessonTwo
{
	static Scanner opScanner = new Scanner(System.in);
	
	public static void main(String args[])
	{
		System.out.print("Your favourite number:");
		
		if(opScanner.hasNextInt())
		{
			int userInput = opScanner.nextInt();
			
			System.out.println("You Entered "+ userInput);
			
			int numEnteredTimes2 = userInput+userInput;
			System.out.println(userInput+" + "+userInput+" = "+numEnteredTimes2);
			
			int numEnteredMinus2 = userInput-2;
			System.out.println(userInput+" - 2 = "+numEnteredMinus2);
			
			int numEnteredTimesSelf = userInput*userInput;
			System.out.println(userInput+" X "+userInput+" = "+numEnteredTimesSelf);
			
			int numEnteredDivideTwo = userInput/2;
			System.out.println(userInput+" / 2 = "+numEnteredDivideTwo);
			
			int numEnteredRemainder = userInput%2;
			System.out.println(userInput+" % 2  = "+numEnteredRemainder);
			
			userInput += 2;//add 2
			userInput -= 2;//minus 2
			
			userInput--;;//minus 1
			userInput++;//add 1
			
			int numberABS = Math.abs(userInput);
			int whichIsBigger = Math.max(5, 7);
			int whichIsSmaller = Math.min(5, 7);
			
			double numSqrt = Math.sqrt(5.23);
			int numCeiling = (int) Math.ceil(5.23);
			int numFloor = (int) Math.floor(5.23);
			int numRound = (int) Math.round(5.2);
			
			int randomNumber = (int) (Math.random()* 11);
			System.out.println("Random Number: "+ randomNumber);
		
		}else
		{
			System.out.println("Enter a Integer next time. ");
		}
	}
}